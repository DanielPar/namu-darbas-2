﻿using System;

namespace NamuDarbasIf
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Ivestike '1' noredami pasirinkti lietuviu kalba");
			Console.WriteLine("Enter '2' for English");
			Console.WriteLine("Введите '3' для русского языка");
			string kalba = Console.ReadLine();
			if (kalba == "1")
			{
				Console.WriteLine("Sveiki!");
			}
			else if (kalba == "2")
			{
				Console.WriteLine("Hello!");
			}
			else if (kalba == "3")
			{
				Console.WriteLine("Здравствуйте!");
			}
			Console.WriteLine("Ivestike savo varda: ");
			string vardas = Console.ReadLine();
			if (string.IsNullOrEmpty(vardas))
			{
				Console.WriteLine("Nieko neivedete");
				Environment.Exit(0);
			}
			Console.WriteLine("Iveskite savo pavarde: ");
			string pavarde = Console.ReadLine();
			if (string.IsNullOrEmpty(pavarde))
			{
				Console.WriteLine("Nieko neivedete");
				Environment.Exit(0);
			}
			Console.WriteLine("Iveskite jusu amziu: ");
			string amziusstring = Console.ReadLine();
			int amzius = Int32.Parse(amziusstring);
			if (amzius <= 0)
			{
				Console.WriteLine("Ivedete neteisinga amziu");
				Environment.Exit(0);
			}
			if (amzius >= 0 && amzius <= 10)
			{
				Console.WriteLine("Ar jau einate i mokykla? [TAIP/NE]");
				string einaimokyklastring = Console.ReadLine();
				bool einaimokykla = einaimokyklastring == "TAIP" && einaimokyklastring == "taip" && einaimokyklastring == "Taip";
			}
			Console.WriteLine("Iveskite savo ugi: ");
			string ugisstring = Console.ReadLine();
			float ugis = float.Parse(ugisstring);
			do
			{
				Console.WriteLine("Ivedete neteisinga ugi");
				Console.WriteLine("Iveskite savo ugi: ");
				ugisstring = Console.ReadLine();
				ugis = float.Parse(ugisstring);
			}
			while (ugis <= 0.3);

			Console.WriteLine("Ar jus esate moteris? [TAIP/NE]");
			string lytisstring = Console.ReadLine();
			bool lytisM = lytisstring == "TAIP" && lytisstring == "taip" && lytisstring == "Taip";


			Console.ReadLine();
		}
	}
}
